FROM maven:3.8.4-openjdk-11-slim

# copy the source tree and the pom.xml to our new container
RUN mkdir /src
WORKDIR /src
COPY . .
# package our application code
RUN mvn -f ./pom.xml clean package

EXPOSE 8080

# set the startup command to execute the jar
CMD ["java", "-jar", "target/quarkus-app/quarkus-run.jar"]