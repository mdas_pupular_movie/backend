SHELL := /bin/bash # Use bash syntax
colon := ":"
registry := registry.gitlab.com/mdas_pupular_movie/api
tag := latest

export
docker_build:
	docker build . -t ${registry}
	docker tag ${registry}${colon}${tag} ${registry}${colon}${tag}
	docker push -a ${registry}

start_prod:
	./mvnw -f ./pom.xml clean package
	java -jar target/quarkus-app/quarkus-run.jar

start_dev:
	./mvnw quarkus:dev
