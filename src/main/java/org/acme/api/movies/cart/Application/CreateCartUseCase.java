package org.acme.api.movies.cart.Application;

import org.acme.api.movies.cart.Domain.Creator.CartCreator;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CreateCartUseCase {
    @Inject
    CartCreator cartCreator;

    public CartId execute(){
        CartId cartId = new CartId();
        cartCreator.create(cartId);
        return cartId;
    }
}
