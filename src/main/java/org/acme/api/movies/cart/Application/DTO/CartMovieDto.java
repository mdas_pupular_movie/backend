package org.acme.api.movies.cart.Application.DTO;

public class CartMovieDto {
    private final String cartId;
    private final String movieId;

    public CartMovieDto(String cartId, String movieId) {
        this.cartId = cartId;
        this.movieId = movieId;
    }

    public String cartId() {
        return cartId;
    }

    public String movieId() {
        return movieId;
    }
}
