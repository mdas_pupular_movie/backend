package org.acme.api.movies.cart.Application;

import org.acme.api.movies.cart.Domain.Converter.CartConverter;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Finder.CartFinder;
import org.acme.api.movies.cart.Domain.Resources.CartResponseResource;
import org.acme.api.movies.cart.Domain.Searcher.MovieCartSearcher;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;

@ApplicationScoped
public class GetCartUseCase {
    @Inject
    CartFinder finder;

    @Inject
    MovieCartSearcher searcher;

    @Inject
    CartConverter converter;

    public CartResponseResource execute(String id) throws CustomException, IOException {
        Cart cart = finder.find(new CartId(id));
        ArrayList<MovieElastic> movies = searcher.search(cart.moviesList().getMovieIds());
        return converter.convert(cart, movies);
    }
}
