package org.acme.api.movies.cart.Application;

import org.acme.api.movies.cart.Domain.Remove.CartRemover;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class RemoveCartUseCase {
    @Inject
    CartRemover remover;

    public void execute(CartId id) throws CustomException {
        remover.remove(id);
    }
}
