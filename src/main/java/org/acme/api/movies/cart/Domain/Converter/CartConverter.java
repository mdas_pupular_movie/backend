package org.acme.api.movies.cart.Domain.Converter;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.Resources.CartResponseResource;
import org.acme.api.movies.cart.Domain.Resources.MovieCartResponseResource;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class CartConverter {
    public CartResponseResource convert(Cart cart, ArrayList<MovieElastic> movies){
        CartResponseResource response = new CartResponseResource();
        response.cartId = cart.cartId().getId();
        response.totalPrice = 0.0;

        int index = 0;
        List<MovieCartResponseResource> movieList = new ArrayList<>();
        for (MovieCart movie : cart.moviesList().movies()) {
            MovieCartResponseResource movieResponse = new MovieCartResponseResource();
            movieResponse.id = movie.movieId();
            movieResponse.title = movie.title();
            movieResponse.price = movies.get(index).price().getPriceValue();
            movieResponse.quantity = movie.quantity().getQuantity();
            movieList.add(movieResponse);
            response.totalPrice += movieResponse.price * movie.quantity().getQuantity();
            index++;
        }

        response.moviesList = movieList;
        return response;
    }
}
