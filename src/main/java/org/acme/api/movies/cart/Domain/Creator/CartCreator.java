package org.acme.api.movies.cart.Domain.Creator;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CartCreator {
    @Inject
    InfinispanCartRepository cartRepository;

    public void create(CartId cartId){
        Cart cart = new Cart(
                cartId,
                new MovieCollection()
        );
        cartRepository.save(cart);
    }
}
