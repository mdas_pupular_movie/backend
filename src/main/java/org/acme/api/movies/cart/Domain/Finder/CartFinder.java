package org.acme.api.movies.cart.Domain.Finder;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CartFinder {
    @Inject
    InfinispanCartRepository cartRepository;

    public Cart find(CartId cartId) throws CustomException {
        this.existCart(cartId);
        return cartRepository.getCart(cartId.getId());
    }

    public void existCart(CartId cartId) throws CustomException {
        if (!cartRepository.exists(cartId)){
            throw new CustomException("Cart not found.", 404);
        }
    }
}
