package org.acme.api.movies.cart.Domain.Finder;

import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieId;
import org.acme.api.movies.cart.Infraestructure.Repositories.ElasticMovieCartRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;

@ApplicationScoped
public class MovieCartFinder {

    @Inject
    ElasticMovieCartRepository repository;

    public MovieElastic find(MovieId movieId) throws IOException, CustomException {
        ArrayList<MovieId> request = new ArrayList<MovieId>();
        request.add(movieId);
        ArrayList<MovieElastic> response = repository.getMovieCart(request);
        this.existMovie(response);
        return response.get(0);
    }

    public void existMovie(ArrayList<MovieElastic> movies) throws CustomException {
        if (movies.size() < 1){
            throw new CustomException("Movie not found.", 404);
        }
    }
}
