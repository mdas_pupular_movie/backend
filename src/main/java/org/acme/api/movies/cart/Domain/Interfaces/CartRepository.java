package org.acme.api.movies.cart.Domain.Interfaces;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;

import java.net.ConnectException;

public interface CartRepository {
  Cart getCart(String cartId);
  void save(Cart cart);
  void update(Cart cart);
  boolean exists(CartId cartId);
  void removeCart(CartId cartId) throws ConnectException;
}
