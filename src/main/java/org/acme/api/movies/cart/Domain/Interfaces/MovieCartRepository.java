package org.acme.api.movies.cart.Domain.Interfaces;

import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieId;

import java.io.IOException;
import java.util.ArrayList;

public interface MovieCartRepository {
      ArrayList<MovieElastic> getMovieCart(ArrayList<MovieId> movieIds) throws IOException;
}
