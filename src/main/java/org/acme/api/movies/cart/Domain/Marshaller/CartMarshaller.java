package org.acme.api.movies.cart.Domain.Marshaller;

import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;
import java.util.ArrayList;

public class CartMarshaller implements MessageMarshaller<Cart> {
    @Override
    public Cart readFrom(ProtoStreamReader reader) throws IOException {
        CartId cartId = new CartId(reader.readString("cartId"));
        MovieCollection moviesList = new MovieCollection(reader.readCollection("moviesList", new ArrayList<>(), MovieCart.class));
        return new Cart(cartId, moviesList);
    }

    @Override
    public void writeTo(ProtoStreamWriter writer, Cart cart) throws IOException {
        writer.writeString("cartId", cart.cartId().getId());
        writer.writeCollection("moviesList", cart.moviesList().movies(), MovieCart.class);
    }

    @Override
    public Class<? extends Cart> getJavaClass() {
        return Cart.class;
    }

    @Override
    public String getTypeName() {
        return "cart_table.Cart";
    }
}
