package org.acme.api.movies.cart.Domain.Marshaller;

import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.Quantity;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;

public class MovieCartMarshaller implements MessageMarshaller<MovieCart> {
    @Override
    public MovieCart readFrom(ProtoStreamReader reader) throws IOException {
        String movieId = reader.readString("movieId");
        String title = reader.readString("title");
        Quantity quantity = new Quantity(reader.readInt("quantity"));
        return new MovieCart(movieId, title, quantity);
    }

    @Override
    public void writeTo(ProtoStreamWriter writer, MovieCart movieCart) throws IOException {
        writer.writeString("movieId", movieCart.movieId());
        writer.writeString("title", movieCart.title());
        writer.writeInt("quantity", movieCart.quantity().getQuantity());
    }

    @Override
    public Class<? extends MovieCart> getJavaClass() {
        return MovieCart.class;
    }

    @Override
    public String getTypeName() {
        return "cart_table.MovieCart";
    }
}
