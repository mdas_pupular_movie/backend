package org.acme.api.movies.cart.Domain.Remove;

import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class CartRemover {
    @Inject
    InfinispanCartRepository cartRepository;

    public void remove(CartId cartId) throws CustomException {
        this.existCart(cartId);
        cartRepository.removeCart(cartId);
    }

    public void existCart(CartId cartId) throws CustomException {
        if (!cartRepository.exists(cartId)){
            throw new CustomException("Cart not found.", 404);
        }
    }
}
