package org.acme.api.movies.cart.Domain.Resources;

import java.util.List;
import java.util.Objects;

public class CartResponseResource {
    public String cartId;
    public List<MovieCartResponseResource> moviesList;
    public Double totalPrice;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartResponseResource)) {
            return false;
        }
        CartResponseResource other = (CartResponseResource) o;
        return Objects.equals(cartId, other.cartId)
                && Objects.equals(totalPrice, other.totalPrice)
                && Objects.equals(moviesList, other.moviesList);
    }
}
