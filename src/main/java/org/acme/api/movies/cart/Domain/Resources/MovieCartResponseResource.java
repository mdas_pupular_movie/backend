package org.acme.api.movies.cart.Domain.Resources;

import java.util.Objects;

public class MovieCartResponseResource {
    public String id;
    public String title;
    public Double price;
    public int quantity;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieCartResponseResource)) {
            return false;
        }
        MovieCartResponseResource other = (MovieCartResponseResource) o;
        return Objects.equals(id, other.id)
                && Objects.equals(title, other.title)
                && Objects.equals(price, other.price)
                && Objects.equals(quantity, other.quantity);
    }
}
