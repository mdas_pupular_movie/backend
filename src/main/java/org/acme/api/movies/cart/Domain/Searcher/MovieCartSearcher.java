package org.acme.api.movies.cart.Domain.Searcher;

import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieId;
import org.acme.api.movies.cart.Infraestructure.Repositories.ElasticMovieCartRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;

@ApplicationScoped
public class MovieCartSearcher {
    @Inject
    ElasticMovieCartRepository repository;

    public ArrayList<MovieElastic> search(ArrayList<MovieId> movieId) throws IOException {
        return repository.getMovieCart(movieId);
    }
}
