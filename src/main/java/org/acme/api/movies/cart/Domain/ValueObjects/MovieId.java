package org.acme.api.movies.cart.Domain.ValueObjects;

public class MovieId {
    private final String id;

    public MovieId(String id) {
        this.id = id;
    }

    public String movieId() {
        return id;
    }
}
