package org.acme.api.movies.cart.Domain.ValueObjects;

import org.infinispan.protostream.annotations.ProtoFactory;
import org.infinispan.protostream.annotations.ProtoField;

public class Price {
    private Double priceValue;

    @ProtoFactory
    public Price(Double priceValue) {
        this.priceValue = priceValue;
    }

    public Price() {
        this.priceValue = 0.0;
    }

    @ProtoField(number = 1)
    public Double getPriceValue() {
        return priceValue;
    }

    public void plusMovie(double moviePrice){
        priceValue += moviePrice;
    }

    public void subtractMovie(double moviePrice){
        priceValue -= moviePrice;
    }
}
