package org.acme.api.movies.cart.Infraestructure.Repositories;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.acme.api.movies.cart.Domain.Interfaces.MovieCartRepository;
import org.acme.api.movies.cart.Domain.Resources.MovieCartResponseResource;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieElastic;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieId;
import org.acme.api.movies.cart.Domain.ValueObjects.Price;
import org.acme.api.movies.cart.Domain.ValueObjects.Quantity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;

@ApplicationScoped
public class ElasticMovieCartRepository implements MovieCartRepository {

    @Inject
    RestClient restClient;

    @Override
    @Transactional
    public ArrayList<MovieElastic> getMovieCart(ArrayList<MovieId> movieIds) throws IOException {
        Request request = new Request(
                "GET",
                "/movies/_search");
        JsonObject matchJson = new JsonObject().put("ids", new JsonObject().put("values", movieIds.stream().map(MovieId::movieId).toArray()));
        JsonObject queryJson = new JsonObject().put("query", matchJson);
        request.setJsonEntity(queryJson.encode());
        Response response = restClient.performRequest(request);
        String responseBody = EntityUtils.toString(response.getEntity());

        JsonObject json = new JsonObject(responseBody);
        JsonArray hits = json.getJsonObject("hits").getJsonArray("hits");

        ArrayList<MovieElastic> results = new ArrayList<MovieElastic>(hits.size());

        for (int i = 0; i < hits.size(); i++) {
            JsonObject hit = hits.getJsonObject(i);
            MovieCartResponseResource movieResources = hit.getJsonObject("_source").mapTo(MovieCartResponseResource.class);
            results.add(new MovieElastic(
                movieResources.id,
                movieResources.title,
                new Price(movieResources.price),
                new Quantity(1)
            ));
        }

        return results;
    }
}
