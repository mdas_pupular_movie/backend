package org.acme.api.movies.movie.Application.DTO;

import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;

public class GetMoviesUseCaseRequest {
    public Pagination pagination;

    public GetMoviesUseCaseRequest(Pagination pagination) {
        this.pagination = pagination;
    }

    public Pagination pagination() {
        return pagination;
    }
}
