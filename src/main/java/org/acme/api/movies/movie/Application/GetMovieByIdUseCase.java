package org.acme.api.movies.movie.Application;

import org.acme.api.movies.movie.Application.DTO.GetMovieByIdRequest;
import org.acme.api.movies.movie.Domain.Converter.MoviesConverter;
import org.acme.api.movies.movie.Domain.Finder.MovieFinder;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class GetMovieByIdUseCase {

    @Inject
    MovieFinder finder;

    @Inject
    MoviesConverter converter;

    public MovieResources execute(GetMovieByIdRequest request) throws IOException, CustomException {
        Movie movie = finder.find(new MovieId(request.movieId()));
        return converter.convert(movie);
    }
}
