package org.acme.api.movies.movie.Application;

import org.acme.api.movies.movie.Application.DTO.GetMoviesUseCaseRequest;
import org.acme.api.movies.movie.Domain.Converter.MoviesListConverter;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.Resources.ResultMovieResources;
import org.acme.api.movies.movie.Domain.Searcher.MoviesSearcher;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class GetMoviesUseCase {

    @Inject
    MoviesSearcher searcher;

    @Inject
    MoviesListConverter listConverter;

    public ResultMovieResources run(GetMoviesUseCaseRequest request) throws IOException, CustomException {
         Movies movies = searcher.execute(request.pagination);
         return listConverter.convert(movies);
    }
}
