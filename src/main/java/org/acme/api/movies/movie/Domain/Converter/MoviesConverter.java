package org.acme.api.movies.movie.Domain.Converter;

import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MoviesConverter {
    public MovieResources convert(Movie movie){
        MovieResources movieResources = new MovieResources();
        movieResources.id = movie.movieId().id();
        movieResources.cover = movie.cover();
        movieResources.description = movie.description();
        movieResources.genreName = movie.genre();
        movieResources.rating = movie.rating();
        movieResources.price = movie.price();
        movieResources.title = movie.title();
        movieResources.year = movie.year();

        return movieResources;
    }
}
