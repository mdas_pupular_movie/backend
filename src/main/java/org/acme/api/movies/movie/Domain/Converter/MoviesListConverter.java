package org.acme.api.movies.movie.Domain.Converter;

import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.Resources.ResultMovieResources;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;

@ApplicationScoped
public class MoviesListConverter {

    @Inject
    MoviesConverter moviesConverter;

    public ResultMovieResources convert(Movies movies){
        ResultMovieResources resultMovieResources = new ResultMovieResources();
        ArrayList<MovieResources> movieArray = new ArrayList<>();
         movies.movies().movieCollection().forEach((movie) -> movieArray.add(moviesConverter.convert(movie)));

         resultMovieResources.results = movieArray;
         resultMovieResources.total_results = movies.totalMovies();
         resultMovieResources.page = movies.pagination().page();
         resultMovieResources.total_pages = ((int) (movies.totalMovies() / movies.pagination().limit())) + 1;

         return resultMovieResources;
    }
}
