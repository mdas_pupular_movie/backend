package org.acme.api.movies.movie.Domain.Finder;

import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Infraestructure.Repositories.ElasticMovieRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class MovieFinder {

    @Inject
    ElasticMovieRepository repository;

    public Movie find(MovieId movieId) throws IOException, CustomException {
        this.existMovie(movieId);
        return repository.getMovieById(movieId);
    }

    public void existMovie(MovieId movieId) throws CustomException, IOException {
        if (!repository.exists(movieId)){
            throw new CustomException("Movie not found.", 404);
        }
    }
}
