package org.acme.api.movies.movie.Domain.Interfaces;

import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;

import java.io.IOException;

public interface MovieRepository {
    Movies getMovies(Pagination pagination) throws IOException;
    Movie getMovieById(MovieId movieId) throws IOException;
    boolean exists(MovieId movieId) throws IOException;
}
