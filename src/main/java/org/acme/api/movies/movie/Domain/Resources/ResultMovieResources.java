package org.acme.api.movies.movie.Domain.Resources;

import java.util.List;
import java.util.Objects;

public class ResultMovieResources {
    public int page;
    public List<MovieResources> results;
    public int total_pages;
    public int total_results;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResultMovieResources)) {
            return false;
        }
        ResultMovieResources other = (ResultMovieResources) o;
        return page == other.page
                && total_pages == other.total_pages
                && total_results == other.total_results
                && Objects.equals(results, other.results);
    }
}
