package org.acme.api.movies.movie.Domain.Searcher;

import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.acme.api.movies.movie.Infraestructure.Repositories.ElasticMovieRepository;
import org.acme.api.shared.Domain.CustomException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;

@ApplicationScoped
public class MoviesSearcher {

    @Inject
    ElasticMovieRepository repository;

    public Movies execute(Pagination pagination) throws IOException, CustomException {
        this.correctPagination(pagination);
        return repository.getMovies(pagination);
    }

    private void correctPagination(Pagination pagination) throws CustomException {
        if(pagination.page < 1){
            throw new CustomException("Incorrect page number", 400);
        }

        if(pagination.limit < 1){
            throw new CustomException("Incorrect number of results", 400);
        }
    }
}
