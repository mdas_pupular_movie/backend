package org.acme.api.movies.movie.Domain.ValueObjects;

import java.util.Date;
import java.util.Objects;

public class Movie {
    private MovieId id;
    private String cover;
    private String title;
    private String genre;
    private float price;
    private Date year;
    private float rating;
    private String description;

    public Movie(MovieId id, String cover, String title, String genre, float price, Date year, float rating, String description) {
        this.id = id;
        this.cover = cover;
        this.title = title;
        this.genre = genre;
        this.price = price;
        this.year = year;
        this.rating = rating;
        this.description = description;
    }

    public MovieId movieId() {
        return id;
    }

    public String cover() {
        return cover;
    }

    public String title() {
        return title;
    }

    public String genre() {
        return genre;
    }

    public float price() {
        return price;
    }

    public Date year() {
        return year;
    }

    public float rating() {
        return rating;
    }

    public String description() {
        return description;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) {
            return false;
        }
        Movie other = (Movie) o;
        return Objects.equals(id, other.id)
                && Objects.equals(cover, other.cover)
                && Objects.equals(title, other.title)
                && Objects.equals(genre, other.genre)
                && price == other.price
                && rating == other.rating
                && Objects.equals(description, other.description);
    }
}
