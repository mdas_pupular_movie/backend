package org.acme.api.movies.movie.Domain.ValueObjects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class MovieCollection {
    private Collection<Movie> movieCollection;

    public MovieCollection() {
        this.movieCollection = new ArrayList<>();
    }

    public MovieCollection(Collection<Movie> movieCollection) {
        this.movieCollection = movieCollection;
    }

    public Collection<Movie> movieCollection() {
        return movieCollection;
    }

    public void addMovie(Movie movie){
        this.movieCollection.add(movie);
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieCollection)) {
            return false;
        }
        MovieCollection other = (MovieCollection) o;
        return Objects.equals(movieCollection, other.movieCollection);
    }
}
