package org.acme.api.movies.movie.Domain.ValueObjects;

import java.util.Objects;

public class MovieId {
    private String id;

    public MovieId(String id) {
        this.id = id;
    }

    public String id() {
        return id;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MovieId)) {
            return false;
        }
        MovieId other = (MovieId) o;
        return Objects.equals(id, other.id);
    }
}
