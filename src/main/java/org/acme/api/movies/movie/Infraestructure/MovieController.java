package org.acme.api.movies.movie.Infraestructure;

import io.vertx.core.json.JsonObject;
import org.acme.api.movies.movie.Application.DTO.GetMovieByIdRequest;
import org.acme.api.movies.movie.Application.DTO.GetMoviesUseCaseRequest;
import org.acme.api.movies.movie.Application.GetMovieByIdUseCase;
import org.acme.api.movies.movie.Application.GetMoviesUseCase;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.Resources.ResultMovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.acme.api.movies.movie.Infraestructure.ControllerRequest.MovieControllerRequest;
import org.acme.api.shared.Domain.CustomException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/movies")
public class MovieController {

    @Inject
    GetMoviesUseCase getMoviesUseCase;

    @Inject
    GetMovieByIdUseCase getMovieByIdUseCase;

    @GET
    @Path("{movieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getMovieById(@PathParam("movieId") String movieId) throws IOException, CustomException {
        MovieResources movie = getMovieByIdUseCase.execute(
                new GetMovieByIdRequest(movieId)
        );
        return JsonObject.mapFrom(movie).toString();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getMovies(MovieControllerRequest request) throws IOException, CustomException {
        ResultMovieResources movies = getMoviesUseCase.run(
                new GetMoviesUseCaseRequest(
                        new Pagination(
                            request.searchTearm,
                            request.page,
                            request.searchBy,
                            request.limit
                        )
                )
        );
        return JsonObject.mapFrom(movies).toString();
    }
}
