package org.acme.api.shared.Domain;

import io.vertx.core.json.JsonObject;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<CustomException> {
    @Override
    public Response toResponse(CustomException exception)
    {
        return Response.status(Response.Status.fromStatusCode(exception.status())).entity(new JsonObject().put("Error",exception.getMessage())).build();
    }
}
