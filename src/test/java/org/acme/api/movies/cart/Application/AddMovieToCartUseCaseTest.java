package org.acme.api.movies.cart.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Application.DTO.CartMovieDto;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.Finder.CartFinder;
import org.acme.api.movies.cart.Domain.Finder.MovieCartFinder;
import org.acme.api.movies.cart.Domain.Saver.CartSaver;
import org.acme.api.movies.cart.Domain.ValueObjects.*;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@QuarkusTest
public class AddMovieToCartUseCaseTest {
    @InjectMock
    CartFinder finder;

    @InjectMock
    CartSaver saver;

    @InjectMock
    MovieCartFinder movieCartFinder;

    @Inject
    AddMovieToCartUseCase useCase;

    private Cart cart;
    private MovieElastic movieElastic;
    private CartMovieDto cartMovieDto;

    @BeforeEach
    void setUp(){
        CartId id = new CartId();
        MovieId idMovie = new MovieId("movieId");
        cartMovieDto = new CartMovieDto(id.getId(), idMovie.movieId());
        movieElastic = new MovieElastic(idMovie.movieId(), "title", new Price(1.0), new Quantity(1));
        cart = new Cart(
                id,
                new MovieCollection()
        );
    }

    @Test
    public void useCaseTestResponse() throws IOException, CustomException {
        when(finder.find(any())).thenReturn(cart);
        when(movieCartFinder.find(any())).thenReturn(movieElastic);
        cart.moviesList().addMovie(new MovieCart(
                movieElastic.movieId(),
                movieElastic.title(),
                movieElastic.quantity()
        ));
        useCase.execute(cartMovieDto);
        verify(finder, times(1)).find(any(CartId.class));
        verify(movieCartFinder, times(1)).find(any(MovieId.class));
        verify(saver, times(1)).save(any(Cart.class));
    }

    @Test
    public void useCaseTestResponseError() throws CustomException {
        doThrow(CustomException.class).when(finder).find(any());
        Assertions.assertThrowsExactly(CustomException.class, () -> useCase.execute(cartMovieDto));
    }
}
