package org.acme.api.movies.cart.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Creator.CartCreator;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

@QuarkusTest
public class CreateCartUseCaseTest {
    @Inject
    CreateCartUseCase useCase;

    @InjectMock
    CartCreator creator;

    @Test
    public void useCaseTestResponse() {
        doNothing().when(creator).create(any(CartId.class));
        CartId result = useCase.execute();
        Assertions.assertInstanceOf(CartId.class, result);
    }
}
