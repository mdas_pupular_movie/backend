package org.acme.api.movies.cart.Domain.Converter;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.Resources.CartResponseResource;
import org.acme.api.movies.cart.Domain.Resources.MovieCartResponseResource;
import org.acme.api.movies.cart.Domain.ValueObjects.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@QuarkusTest
public class CartConverterTest {
    @Inject
    CartConverter converter;

    @Test
    public void converterCart() {
        MovieElastic movieElastic = new MovieElastic(
                "movieId",
                "title",
                new Price(5.0),
                new Quantity(1)
        );

        ArrayList<MovieElastic> movies = new ArrayList<>();
        movies.add(movieElastic);

        MovieCollection movieCollection = new MovieCollection();
        movieCollection.addMovie(new MovieCart(
                "movieId",
                "title",
                new Quantity(1)
        ));

        Cart cart = new Cart(
          new CartId("1"),
                movieCollection
        );

        MovieCartResponseResource movieResponse = new MovieCartResponseResource();
        movieResponse.id = "movieId";
        movieResponse.title = "title";
        movieResponse.price = 5.0;
        movieResponse.quantity = 1;

        List<MovieCartResponseResource> movieList = new ArrayList<>();
        movieList.add(movieResponse);

        CartResponseResource response = new CartResponseResource();
        response.cartId = "1";
        response.totalPrice = 5.0;
        response.moviesList = movieList;

        CartResponseResource result = converter.convert(cart, movies);

        Assertions.assertEquals(result,response);
    }
}
