package org.acme.api.movies.cart.Domain.Finder;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

@QuarkusTest
public class CartFinderTest {

    @InjectMock
    InfinispanCartRepository repository;

    @Inject
    CartFinder finder;

    private Cart cart;

    @BeforeEach
    void setUp() {
        cart = new Cart(
            new CartId("1"),
            new MovieCollection()
        );
    }

    @Test
    public void finderTestResponse() throws CustomException {
        CartId cartId = new CartId("1");
        Mockito.when(repository.exists(cartId)).thenReturn(true);
        Mockito.when(repository.getCart(cartId.getId())).thenReturn(cart);
        Cart result = finder.find(cartId);
        Assertions.assertEquals(result, cart);
    }

    @Test
    public void finderTestResponseFail() throws CustomException {
        CartId cartId = new CartId("1");
        Mockito.when(repository.exists(cartId)).thenReturn(false);
        Assertions.assertThrowsExactly(CustomException.class, () -> finder.find(cartId));
    }

}
