package org.acme.api.movies.cart.Domain.Remove;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.any;

@QuarkusTest
public class CartRemoverTest {

    @InjectMock
    InfinispanCartRepository repository;

    @InjectMock
    CartRemover remover;

    @Test
    public void removerResponseTest() throws CustomException {

        Cart cart = new Cart(
                new CartId("1"),
                new MovieCollection()
        );

        Mockito.when(repository.exists(cart.cartId())).thenReturn(true);
        remover.remove(cart.cartId());
        verify(remover, times(1)).remove(any());
    }

}
