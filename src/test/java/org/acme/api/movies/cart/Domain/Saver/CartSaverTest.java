package org.acme.api.movies.cart.Domain.Saver;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.cart.Domain.Entity.Cart;
import org.acme.api.movies.cart.Domain.Entity.MovieCart;
import org.acme.api.movies.cart.Domain.ValueObjects.CartId;
import org.acme.api.movies.cart.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.cart.Domain.ValueObjects.Quantity;
import org.acme.api.movies.cart.Infraestructure.Repositories.InfinispanCartRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@QuarkusTest
public class CartSaverTest {

    @InjectMock
    InfinispanCartRepository repository;

    @Inject
    CartSaver saver;

    private Cart cart;

    @BeforeEach
    void setUp() {
        cart = new Cart(
                new CartId("1"),
                new MovieCollection()
        );
    }

    @Test
    public void saverTestResponse() throws CustomException {

        cart.moviesList().addMovie(new MovieCart(
                "7qop80YfuO0BwJa1uXk1DXUUEwv",
                "The Bad Guys",
                new Quantity(1)
        ));

        Mockito.when(repository.exists(cart.cartId())).thenReturn(true);
        doNothing().when(repository).update(any());

        saver.save(cart);
        verify(repository, times(1)).update(any());

    }

    @Test
    public void saverTestFail() {

       CartId cartIdNotExist = new CartId("2");
       Mockito.when(repository.exists(cartIdNotExist)).thenReturn(false);
       Assertions.assertThrowsExactly(CustomException.class, () -> saver.save(cart));

    }



}
