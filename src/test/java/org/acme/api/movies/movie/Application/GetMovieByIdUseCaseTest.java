package org.acme.api.movies.movie.Application;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.movie.Application.DTO.GetMovieByIdRequest;
import org.acme.api.movies.movie.Domain.Converter.MoviesConverter;
import org.acme.api.movies.movie.Domain.Finder.MovieFinder;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@QuarkusTest
public class GetMovieByIdUseCaseTest {
  @Inject
  GetMovieByIdUseCase useCase;

  @InjectMock
  MovieFinder finder;

  @InjectMock
  MoviesConverter converter;

  private MovieResources resources;

  private final GetMovieByIdRequest request = new GetMovieByIdRequest("id");
  private final MovieId id = new MovieId(request.movieId());

  @BeforeEach
  void setUp() throws IOException, CustomException {
    Movie movie = new Movie(
            id,
            "cover",
            "Fantasy Movie",
            "Fantasy",
            2.0F,
            new Date(2022, Calendar.MARCH, 1),
            (float) 1.4,
            "Description fantasy movie"
    );

    resources = new MovieResources();
    resources.id = "id";
    resources.cover = "cover";
    resources.description = "description";
    resources.genreName = "Horror";
    resources.rating = 5.0F;
    resources.price = (float) 0.9;
    resources.title = "title";
    resources.year = new Date(2022, Calendar.APRIL,12);

    when(finder.find(any())).thenReturn(movie);

    when(converter.convert(movie)).thenReturn(resources);
  }

  @Test
  public void useCaseTestResponse() throws IOException, CustomException {
    MovieResources result = useCase.execute(request);
    Assertions.assertEquals(resources, result);
  }

  @Test
  public void useCaseTestResponseFail() throws IOException, CustomException {
    when(finder.find(any())).thenThrow(CustomException.class);
    Assertions.assertThrowsExactly(CustomException.class, () -> useCase.execute(request));
  }
}
