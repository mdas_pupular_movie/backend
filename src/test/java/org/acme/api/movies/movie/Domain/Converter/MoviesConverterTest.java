package org.acme.api.movies.movie.Domain.Converter;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;

@QuarkusTest
public class MoviesConverterTest {
    @Inject
    MoviesConverter converter;

    @Test
    public void converter() {
        Movie movie = new Movie(
                new MovieId("id"),
                "cover",
                "title",
                "Horror",
                (float) 0.9,
                new Date(2022, Calendar.APRIL,12),
                5.0F,
                "description"
        );

        MovieResources response = new MovieResources();
        response.id = "id";
        response.cover = "cover";
        response.description = "description";
        response.genreName = "Horror";
        response.rating = 5.0F;
        response.price = (float) 0.9;
        response.title = "title";
        response.year = new Date(2022, Calendar.APRIL,12);

        MovieResources result = converter.convert(movie);
        Assertions.assertEquals(result,response);
    }
}
