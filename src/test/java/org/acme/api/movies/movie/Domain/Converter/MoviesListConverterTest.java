package org.acme.api.movies.movie.Domain.Converter;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.Resources.MovieResources;
import org.acme.api.movies.movie.Domain.Resources.ResultMovieResources;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@QuarkusTest
public class MoviesListConverterTest {
    @Inject
    MoviesListConverter converter;

    @Test
    public void converterList() {
        Movie movie = new Movie(
                new MovieId("id"),
                "cover",
                "title",
                "Horror",
                (float) 0.9,
                new Date(2022, Calendar.APRIL,12),
                5.0F,
                "description"
        );

        MovieCollection movieCollection = new MovieCollection();
        movieCollection.addMovie(movie);

        Pagination pagination = new Pagination("", 1, "", 10);

        Movies movies = new Movies(
            movieCollection,
                pagination,
                1
        );

        MovieResources movieResources = new MovieResources();
        movieResources.id = "id";
        movieResources.cover = "cover";
        movieResources.description = "description";
        movieResources.genreName = "Horror";
        movieResources.rating = 5.0F;
        movieResources.price = (float) 0.9;
        movieResources.title = "title";
        movieResources.year = new Date(2022, Calendar.APRIL,12);

        ResultMovieResources response = new ResultMovieResources();

        List<MovieResources> movieArray = new ArrayList<>();
        movieArray.add(movieResources);

        response.results = movieArray;
        response.total_results = 1;
        response.page = 1;
        response.total_pages = 1;


        ResultMovieResources result = converter.convert(movies);
        Assertions.assertEquals(result, response);
    }
}
