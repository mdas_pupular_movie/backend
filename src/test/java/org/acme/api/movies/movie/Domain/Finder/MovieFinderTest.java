package org.acme.api.movies.movie.Domain.Finder;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Infraestructure.Repositories.ElasticMovieRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@QuarkusTest
public class MovieFinderTest {

  @InjectMock
  ElasticMovieRepository movieRepository;

  @Inject
  MovieFinder finder;

  private Movie movie;

  @BeforeEach
  void setUp() {
    movie = new Movie(
            new MovieId("id"),
                "cover",
                "Fantasy Movie",
                "Fantasy",
                2.0F,
                new Date(2022, Calendar.MARCH, 1),
                (float) 1.4,
                "Description fantasy movie"
    );
  }

  @Test
  public void finderTestResponse() throws IOException, CustomException {
    MovieId id = new MovieId("id");
    Mockito.when(movieRepository.exists(id)).thenReturn(true);
    Mockito.when(movieRepository.getMovieById(id)).thenReturn(movie);
    Movie result = finder.find(id);
    Assertions.assertEquals(result, movie);
  }

  @Test
  public void finderTestResponseFail() throws IOException {
    MovieId id = new MovieId("id");
    Mockito.when(movieRepository.exists(id)).thenReturn(false);
    Assertions.assertThrowsExactly(CustomException.class, () -> finder.find(id));
  }

}
