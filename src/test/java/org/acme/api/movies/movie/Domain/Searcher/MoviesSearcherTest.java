package org.acme.api.movies.movie.Domain.Searcher;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.acme.api.movies.movie.Infraestructure.Repositories.ElasticMovieRepository;
import org.acme.api.shared.Domain.CustomException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@QuarkusTest
public class MoviesSearcherTest {

  @InjectMock
  ElasticMovieRepository movieRepository;

  @Inject
  MoviesSearcher searcher;

  private Movies movies;
  private Pagination pagination;

  @BeforeEach
  void setUp() {
    Movie movie = new Movie(
            new MovieId("id"),
                "cover",
                "Fantasy Movie",
                "Fantasy",
                2.0F,
                new Date(2022, Calendar.MARCH, 1),
                (float) 1.4,
                "Description fantasy movie"
    );
    MovieCollection collection = new MovieCollection();
    collection.addMovie(movie);
    pagination = new Pagination("", 1, "", 1);
    movies = new Movies(collection, pagination, 1);
  }

  @Test
  public void finderTestResponse() throws IOException, CustomException {
    Mockito.when(movieRepository.getMovies(pagination)).thenReturn(movies);
    Movies result = searcher.execute(pagination);
    Assertions.assertEquals(result, movies);
  }
}
