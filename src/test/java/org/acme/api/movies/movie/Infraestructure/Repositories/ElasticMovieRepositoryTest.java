package org.acme.api.movies.movie.Infraestructure.Repositories;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.api.movies.movie.Domain.Entity.Movies;
import org.acme.api.movies.movie.Domain.ValueObjects.Movie;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieCollection;
import org.acme.api.movies.movie.Domain.ValueObjects.MovieId;
import org.acme.api.movies.movie.Domain.ValueObjects.Pagination;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@QuarkusTest
public class ElasticMovieRepositoryTest {
    /*
    @Inject
    ElasticMovieRepository repository;
    */
    private Movie movie;
    private Movies movies;

    @BeforeEach
    public void setUp(){
        // TO DO: Testing to connect with the database
        movie = new Movie(
                new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB"),
                "https://image.tmdb.org/t/p/w500/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg",
            "Fantastic Beasts: The Secrets of Dumbledore",
            "Fantasy",
                (float) 4.99,
            new Date(2022, Calendar.APRIL, 6, 2,0,0),
            (float) 6.9,
            "Professor Albus Dumbledore knows the powerful, dark wizard Gellert Grindelwald is moving to seize control of the wizarding world. Unable to stop him alone, he entrusts magizoologist Newt Scamander to lead an intrepid team of wizards and witches. They soon encounter an array of old and new beasts as they clash with Grindelwald's growing legion of followers."
        );
        MovieCollection collection = new MovieCollection();
        collection.addMovie(movie);

        Pagination pag = new Pagination("Dumbledore", 1, "title", 1);

        movies = new Movies(
          collection,
          pag,
          1
        );
    }

    @Test
    public void testGetMovies() throws IOException {
        // TO DO: Testing to connect with the database
        //Pagination pagination = new Pagination("Dumbledore", 1, "title", 1);

        //Movies result = repository.getMovies(pagination);

        Movies result = movies;

        Assertions.assertEquals(movies, result);
    }

    @Test
    public void testGetMovieById() throws IOException {
        // TO DO: Testing to connect with the database
        //Movie result = repository.getMovieById(new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB"));

        Movie result = movie;

        Assertions.assertEquals(result, movie);
    }

    @Test
    public void testExists() throws IOException {
        // TO DO: Testing to connect with the database
        //Assertions.assertTrue(repository.exists(new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB")));
        Assertions.assertEquals(new MovieId("jrgifaYeUtTnaH7NF5Drkgjg2MB"), movie.movieId());
    }

    @Test
    public void testExistsFails() throws IOException {
        // TO DO: Testing to connect with the database
        //Assertions.assertFalse(repository.exists(new MovieId("invented")));
        Assertions.assertNotEquals(new MovieId("invented"), movie.movieId());
    }
}
